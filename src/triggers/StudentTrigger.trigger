trigger StudentTrigger on Student__c (after insert, after update, before insert, before update)
{
	fflib_SObjectDomain.triggerHandler(Students.class);
}