global class AccountProcessor {
//    @future
    public static void countContacts(List<Id> accountIds){
        List<Account> accounts = new List<Account>();
        for(Account acc : [select id,(select id, name from Contacts) from account where id in :accountIds]){
            acc.Number_of_Contacts__c = acc.Contacts.size();
            accounts.add(acc);
        }
        update accounts;
    }
}