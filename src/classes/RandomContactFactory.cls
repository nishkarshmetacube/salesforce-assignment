public class RandomContactFactory {

    public static List<Contact> generateRandomContacts(Integer count, String lName){
        List<Contact> contactList = new List<Contact>();
        for(Integer i =0;i<count;i++){
            Contact c = new Contact(firstName='Test '+i,lastName=lName);
            contactList.add(c);
        }
        return contactList;
    }
}