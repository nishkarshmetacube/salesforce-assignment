public with sharing class EmailManager
{  
    public static void sendMail(String[] addresses, String subject, String message)
    {
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setSubject(subject);
        email.setToAddresses(addresses);
        email.setPlainTextBody(message);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});
    }

    private static Messaging.SingleEmailMessage getOppUpdateMail(EmailTemplate eTemp, Opportunity opp)
    {
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage(); 
        email.setSaveAsActivity(false);
        email.setTemplateId(eTemp.Id);
        email.setTargetObjectId(opp.OwnerId);
//        email.setWhatId(opp.Id);
		return email;
    }

    public static void sendOppUpdateMails(List<Id> oppIds)
    {
        List<Opportunity> opps = [SELECT Name, Custom_Status__c, OwnerId FROM Opportunity WHERE Id IN :oppIds];
        EmailTemplate eTemp = [SELECT Name FROM EmailTemplate WHERE Name = 'Opportunity status changed' AND isActive = true LIMIT 1];
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        for(Opportunity opp : opps)
        {
            Messaging.SingleEmailMessage email = getOppUpdateMail(eTemp, opp);
            mails.add(email);
        }
        try
        {
	        Messaging.sendEmail(mails);
        }
        catch(Exception e)
        {
            System.debug(e);
        }
    }
}