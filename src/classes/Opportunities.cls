public class Opportunities extends fflib_SObjectDomain
{
    public Opportunities(List<Opportunity> sObjectList)
    {
        super(sObjectList);
    }
    public class Constructor implements fflib_SObjectDomain.IConstructable
    {
        public fflib_SObjectDomain construct(List<SObject> sObjectList)
        {
            return new Opportunities(sObjectList);
        }
    }

    public override void onBeforeUpdate(Map<Id,SObject> existingRecords) 
    {
        updateManager(records);
        for(Opportunity opp : (List<Opportunity>) records) 
        {
            Opportunity existingOpportunity = (Opportunity) existingRecords.get(opp.Id);
            if(String.isNotBlank(opp.StageName) && (opp.StageName.equals('Closed Won') || opp.StageName.equals('Closed Lost')) && !opp.StageName.equals(existingOpportunity.StageName))
            {
                opp.CloseDate = Date.today();
            }
        }
    }
    
    private void updateManager(List<Opportunity> opps){
        Map<Opportunity,Id> opportunitiesMap = new Map<Opportunity,Id>();
		for(Opportunity opp : (List<Opportunity>) records) 
	    {
            if(opp.BillToContact__c != null && opp.Manager__c == null)
            {
                opportunitiesMap.put(opp, opp.BillToContact__c);
            }
        }

        if(opportunitiesMap.size() > 0)
        {
            Map<Id,Contact> contactMap = new Map<Id,Contact>([SELECT Account.Name FROM Contact WHERE Id = :opportunitiesMap.values()]);
            for(Opportunity opp : opportunitiesMap.keySet())
            {
                Id contactId = opportunitiesMap.get(opp);
                opp.Manager__c = contactMap.get(contactId).AccountId;
            }
        }
    }

    public override void onAfterUpdate(Map<Id,SObject> existingRecords) 
    {
        List<Id> oppIds = new List<Id>();
        List<Id> updatedoppIds = new List<Id>();
        for(Opportunity opp : (List<Opportunity>) records)
        {
            Opportunity existingOpp = (Opportunity) existingRecords.get(opp.Id);
            if(String.isNotBlank(opp.Custom_Status__c) && !opp.Custom_Status__c.equals(existingOpp.Custom_Status__c))
            {
                updatedoppIds.add(opp.Id);
                if(opp.Custom_Status__c.equals('Reset'))
                {
					oppIds.add(opp.Id);
                }                
            }
        }
        if(updatedoppIds.size() > 0)
        {
            EmailManager.sendOppUpdateMails(updatedoppIds);
        }
        if(oppIds.size() > 0)
        {
            List<OpportunityLineItem> products = [SELECT Name FROM OpportunityLineItem WHERE Opportunity.Id IN :oppIds];
            if(products.size() > 0)
            {
	            delete products;
            }
        }
    }
}