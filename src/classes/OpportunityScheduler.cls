global class OpportunityScheduler implements Schedulable {
    global void execute(SchedulableContext sc){
        String[] addresses = new String[]{};
        for(Opportunity opp : [SELECT Owner.Email FROM Opportunity WHERE LastModifiedDate < LAST_N_DAYS:30])
        {
            String mailId = opp.Owner.Email;
            addresses.add(mailId);
        }
        EmailManager.sendMail(addresses, 'Opportunity not updated', 'Opportunity not updated from last 30 days.');        
    }
}