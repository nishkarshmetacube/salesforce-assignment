@isTest
private class TestVerifyDate {
        static Date myDate = Date.newInstance(2018, 1, 1);

    @isTest static void testNextMonthDate(){
		Date newDate = mydate.addDays(60);
        Date actualDate = VerifyDate.CheckDates(myDate,newDate);
        System.assertEquals(Date.newInstance(2018, 1, 31), actualDate);
    }

    @isTest static void testCurrentMonthDate(){
		Date newDate = mydate.addDays(20);
        Date actualDate = VerifyDate.CheckDates(myDate,newDate);
        System.assertEquals(Date.newInstance(2018, 1, 21), actualDate);
    }
    
    
}