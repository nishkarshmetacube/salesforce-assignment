@isTest
private class TestProductListingController
{
    @isTest static void test()
    {
        List<Product__c> products = createProducts();
        Contact con = createContact();
        PageReference page = Page.ProductListingPage;
        Test.setCurrentPage(page);
        page.getParameters().put('id',con.Id);
        ProductListingController controller = new ProductListingController();

        // check products count of controller are equal to insert products in DB.
        System.assertEquals(products.size(), controller.getProducts().size());

        Product__c expectedProduct = products.get(0);
        controller.productToAdd = expectedProduct.Id;
        controller.addProduct();
        OrderLineItem actualProduct = controller.productsLineItemMap.get(expectedProduct.Id);
        // check product has been added or not
        System.assert(actualProduct != null);
        System.assertEquals(1, controller.tempLineItems.size());

        // check added product having correct values
        System.assertEquals(expectedProduct.Id, actualProduct.productId);
        System.assertEquals(expectedProduct.Name, actualProduct.Name);
        System.assertEquals(expectedProduct.Price__c, actualProduct.totalPrice);
        System.assertEquals(expectedProduct.Price__c, controller.bagCost);

        // check warning message if we add a product again.
        controller.addProduct();
        System.assert(checkApexPageMessage('Product has been already added.'));

        // check warning message if we increase product quantity more than available count.
        controller.increaseQuantity();
        System.assert(checkApexPageMessage('Order quantity must be smaller than available quantity.'));

        // If we decrease product quantity to 0, then it will be removed from map.
        controller.decreaseQuantity();
        System.assertEquals(0, controller.tempLineItems.size());

        expectedProduct = products.get(1);
        controller.productToAdd = expectedProduct.Id;
        controller.addProduct();
        controller.increaseQuantity();
        System.assertEquals((expectedProduct.Price__c * 2), controller.bagCost);
        actualProduct = controller.productsLineItemMap.get(expectedProduct.Id);
        System.assertEquals(2, actualProduct.quantity);
        System.assertEquals((expectedProduct.Price__c * 2), actualProduct.totalPrice);

        controller.decreaseQuantity();
        System.assertEquals(expectedProduct.Price__c, controller.bagCost);
        actualProduct = controller.productsLineItemMap.get(expectedProduct.Id);
        System.assertEquals(1, actualProduct.quantity);
        System.assertEquals(expectedProduct.Price__c, actualProduct.totalPrice);

        controller.increaseQuantity();
		page = controller.placeOrder();
        System.debug('Url : ' + page.getUrl());
		System.assert(page.getUrl().contains('orderpage'));

        Purchase_Order__c order = controller.order;
        // check contact object is same as given in url
        System.assertEquals(con.Id, order.Contact__c);
		System.assertEquals(con.LastName, order.Contact__r.Name);
        order.Address__c = 'abc';
		controller.savePersonalDetail();
        System.assertEquals(2, controller.order.Number_of_Line_Items__c);
        System.assertEquals((expectedProduct.Price__c * 2), controller.order.Total_Price__c);
        expectedProduct = [SELECT Available_Quantity__c FROM Product__c WHERE Id = :expectedProduct.Id];
        System.assertEquals(0, expectedProduct.Available_Quantity__c);
        System.assert(checkApexPageMessage('Congratulations !! Your order has been placed successfully.'));

        // try to place duplicate order
        controller.savePersonalDetail();
        System.assert(checkApexPageMessage('Sorry !! Some exception occured. Please place order again.'));

        // Go to homepage
		page = controller.homePage();
        System.assert(page.getUrl().contains('productlistingpage'));
        System.assert(controller.orderLineItems == null);
        System.assertEquals(0, controller.bagCost);
        System.assertEquals(products.size() - 1, controller.getProducts().size());
    }

    private static Boolean checkApexPageMessage(String alertMsg)
    {
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        for(Apexpages.Message msg : msgs)
        {
            if (msg.getDetail().equals(alertMsg)) return true;
        }
        return false;
    }

    @isTest static void testWithoutContact()
    {
        String errorMsg = '';
        try
        {
            ProductListingController controller = new ProductListingController();
        }
        catch(Exception e)
        {
            errorMsg = e.getMessage();
        }
        System.assertEquals('Please provide Contact Id in URL.', errorMsg);
    }

    @isTest static void testWithInvalidContact()
    {
        String errorMsg = '';
        try
        {
            PageReference page = Page.ProductListingPage;
            Test.setCurrentPage(page);
            page.getParameters().put('id','xyz');
            ProductListingController controller = new ProductListingController();
        }
        catch(Exception e)
        {
            errorMsg = e.getMessage();
        }
        System.assertEquals('Please provide valid Contact Id.', errorMsg);
    }

    private static Contact createContact()
    {
        Account acc = DataFactory.getDummyAccount('Nishkarsh');
        insert acc;
        Contact con = DataFactory.getDummyContact('Manav', acc.Id);
        insert con;
        return con;
    }

    private static List<Product__c> createProducts()
    {
        List<Product__c> products = new List<Product__c>();
        for(Integer i = 1; i <= 5; i++)
        {
	        Product__c prod = new Product__c();
            prod.Name = 'Product '+i;
            prod.Price__c = 500 * i;
            prod.Available_Quantity__c = i;
            products.add(prod);
        }
        insert products;
        return products;
    }
}