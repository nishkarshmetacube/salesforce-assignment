@isTest(seeAllData=true)
private class TestOpportunities
{
    @isTest static void testUpdateManagerTrigger()
    {
        Contact con = prepareData();
        Opportunity opp = DataFactory.getDummyOpportunity('Manav');
        opp.BillToContact__c = con.Id;
        insert opp;
        opp.Name = 'Jai';
        Database.SaveResult result = Database.update(opp);
        Opportunity updatedOpp = [SELECT Manager__c FROM Opportunity WHERE Id = :opp.Id];
		System.assert(result.isSuccess());
        System.assert(updatedOpp.Manager__c == con.AccountId);
    }

	@isTest static void testClosedOppsDate()
    {
        Opportunity opp = DataFactory.getDummyOpportunity('Manav');
        insert opp;
        opp.StageName = 'Closed Won';
        Database.SaveResult result = Database.update(opp);
        Opportunity updatedOpp = [SELECT CloseDate FROM Opportunity WHERE Id = :opp.Id];
		System.assert(result.isSuccess());
        System.assert(updatedOpp.CloseDate == Date.today());

		opp = DataFactory.getDummyOpportunity('Manav');
        insert opp;
        opp.StageName = 'Closed Lost';
        result = Database.update(opp);
        updatedOpp = [SELECT CloseDate FROM Opportunity WHERE Id = :opp.Id];
		System.assert(result.isSuccess());
        System.assert(updatedOpp.CloseDate == Date.today());
    }

    @isTest static void testResetStatus()
    {
        Opportunity opp = prepareOppDataWithProducts();
        opp.Custom_Status__c = 'Reset';
        update opp;
        List<OpportunityLineItem> assosiatedProd = [SELECT Name From OpportunityLineItem WHERE OpportunityId = :opp.Id];
        System.assert(assosiatedProd.size() == 0);
    }

    private static Opportunity prepareOppDataWithProducts()
    {
        Account acc = DataFactory.getDummyAccount('Nishkarsh');
        insert acc;

        //get standard pricebook
        Pricebook2  standardPb = [SELECT Id, Name, IsActive FROM Pricebook2 WHERE IsStandard = True LIMIT 1];

        Pricebook2 pbk1 = new Pricebook2 (Name = 'Test Pricebook Entry 1', Description = 'Test Pricebook Entry 1', IsActive = true);
        insert pbk1;

        Product2 prd1 = new Product2 (Name = 'Test Product Entry 1', Description = 'Test Product Entry 1', ProductCode = 'ABC', IsActive = true);
        insert prd1;

        PricebookEntry pbe1 = new PricebookEntry (Product2ID = prd1.id, Pricebook2ID = standardPb.id, UnitPrice = 50, isActive = true);
        insert pbe1;

        Opportunity opp1 = new Opportunity (Name = 'Opp1', StageName = 'Stage 0 - Lead Handed Off', CloseDate = Date.today(), Pricebook2Id = pbe1.Pricebook2Id, AccountId = acc.id);
        insert opp1;

        OpportunityLineItem lineItem1 = new OpportunityLineItem (OpportunityID = opp1.id, PriceBookEntryID = pbe1.id, quantity = 4, totalprice = 200);
        insert lineItem1;
		return opp1;
    }

    private static Contact prepareData()
    {
        Account acc = DataFactory.getDummyAccount('Nishkarsh');
        insert acc;
        Contact con = DataFactory.getDummyContact('Manav', acc.Id);
        insert con;
        return con;
    }
}