@isTest
private class TestClassManagerController
{
    @isTest static void test()
    {
        List<Class__c> classes = prepareData();
        PageReference page = Page.ClassManager;
		Test.setCurrentPage(page);
        ClassManagerController controller = new ClassManagerController();
        System.assertEquals(classes.size(), controller.getClasses().size());

        Id classId = classes.get(0).Id;
        page.getParameters().put('classId', classId);
        controller.setClass();
        System.assertEquals(classes.get(0).Id, classId);

        controller.classObj.Name = 'BA';
        controller.save();
        Class__c updatedClass = [SELECT Name FROM Class__c WHERE Id = :classId];
        System.assertEquals('BA', updatedClass.Name);
        System.assert(controller.classObj == null);

        controller.setClass();
		controller.deleteClass();
        System.assert(controller.classObj == null);
        System.assertEquals(1, controller.getClasses().size());
    }

    private static List<Class__c> prepareData()
    {
        List<Class__c> classes = new List<Class__c>();
        Class__c class1 = DataFactory.getDummyClass('BCA');
        classes.add(class1);

        Class__c class2 = DataFactory.getDummyClass('MCA');
        classes.add(class2);
        insert classes;
        return classes;
    }
}