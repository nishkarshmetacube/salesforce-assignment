@isTest
private class TestStack
{
    @isTest static void testPushBeyondCapacity()
    {
        Stack st = getSampleStack();
        String errorMsg = '';
        try
        {
            st.push(9845);
        }
        catch(Exception e)
        {
            errorMsg = e.getMessage();
        }
        System.assertEquals('Stack is Full.', errorMsg);
    }

    @isTest static void testPopBeyondCapacity()
    {
        Stack st = getSampleStack();
        String errorMsg = '';
        st.pop();
        st.pop();
        st.pop();
        try
        {
            st.pop();
        }
        catch(Exception e)
        {
            errorMsg = e.getMessage();
        }
        System.assertEquals('Stack is Empty.', errorMsg);
    }

    @isTest static void testPeek()
    {
        Stack st = getSampleStack();
        System.assertEquals(34, st.peek());
    }
    
    private static Stack getSampleStack()
    {
        Stack st = new Stack(3);
        st.push(1567);
        st.push(1234);
        st.push(34);
		return st;
    }
}