@isTest
private class AccountProcessorTest {
    @isTest static void checkAccountWithContacts(){
        Test.startTest();
        List<Contact> conss = new List<Contact>();
        Account acc = new Account(Name='Neha');
        insert acc;
        Contact cont1 = new Contact(lastName = 'Nishkarsh',AccountId=acc.id);
        Contact cont2 = new Contact(lastName = 'Manav',AccountId=acc.id);
        conss.add(cont1);
        conss.add(cont2);
        insert conss;
        List<Account> accounts = [select id from account where id = :acc.Id];
        System.debug('Size : '+accounts.size());
        AccountProcessor.countContacts(new List<Id>{acc.Id});
        Test.stopTest();
        Account acc1 = [select id,name,(select name from contacts),Number_of_Contacts__c from account where id = :acc.Id];
        System.debug(acc1.contacts);
        System.assertEquals(2, acc1.Number_of_Contacts__c);
    }
    
    @isTest static void checkAccountWithNoContacts(){
        Test.startTest();
        Account acc = new Account(Name='Neha');
        insert acc;
        AccountProcessor.countContacts(new List<Id>{acc.Id});
        Test.stopTest();
        Account acc1 = [select Number_of_Contacts__c from account where id = :acc.Id];
        System.assertEquals(0, acc1.Number_of_Contacts__c);
    }

}