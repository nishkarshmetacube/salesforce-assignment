public class JsonDataController 
{

    public String jsonString {get;set;}

    public JsonDataController()
    {
        jsonString = getSampleData();
    }
    
    private String getSampleData()
    {
        List<Student> students = new List<Student>();
        Student studentObj = new Student();
        studentObj.name = 'Abhishek Gautam';
        studentObj.fatherName = 'Jai Prakash';
        School school = new School();
        school.name = 'Manav Bharti School';
        studentObj.school = school;
		students.add(studentObj);

        Student studentObj1 = new Student();
        studentObj1.name = 'Happy Singh';
        studentObj1.fatherName = 'Joginder Singh';
        studentObj1.school = school;
		students.add(studentObj1);

        return JSON.serialize(students);
    }
}