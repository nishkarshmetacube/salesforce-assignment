public class ContactsGridController
{
    private static final String ASCENDING_ORDER = 'asc';
	private static final String DESCENDING_ORDER = 'desc';
    public List<String> SelectedFields {get; set;}
    public ApexPages.StandardSetController con { get; set; }
    public Integer pageToOpen
    {
        get
        {
            if(pageToOpen == null) pageToOpen = 1; return pageToOpen;
        }
        set;
    }
    public List<SObject> contacts
    {
        get
        {
            if(con == null)
            {
                return new List<SObject>();
            }
            return con.getRecords();
        }        
        set;
    }
    public String query { get;set; }
    // the current sort direction. defaults to asc
    public String sortDir
    {
	    get  { if (sortDir == null) {  sortDir = ASCENDING_ORDER; } return sortDir;  }
    	set;
    }

    // the current field to sort by. defaults to last name
    public String sortField
    {
	    get  { if (sortField == null) {sortField = 'Name'; } return sortField;  }
    	set;
    }

    // toggles the sorting of query from asc<-->desc
    public void toggleSort()
    {
        // simply toggle the direction
        sortDir = sortDir.equals(ASCENDING_ORDER) ? DESCENDING_ORDER : ASCENDING_ORDER;
        // run the query again
        runQuery();
    }

    public void runQuery()
    {
        System.debug('Quer : ' + query);
        String q = query + ' ORDER BY ' + sortField + ' ' + sortDir;
        try
        {
            con = new ApexPages.StandardSetController(Database.getQueryLocator(q));
            con.setPageSize(10);
            if(con.getRecords().size() > 0)
            {
                SObject obj = con.getRecords().get(0);
                SelectedFields = new List<String>(obj.getPopulatedFieldsAsMap().keySet());
            }
        }
        catch (Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Ooops! Some mistake in query.'));
        }
    }

   	// returns the first page of records
 	public void first() 
    {
 		con.first();
 	}

 	// returns the last page of records
 	public void last() 
    {
 		con.last();
 	}

    public Boolean hasNext  
    {  
        get  
        {
            return con.getHasNext();  
        }
        set;  
    }
    public Boolean hasPrevious  
    {  
        get  
        {  
            return con.getHasPrevious();  
        }  
        set;  
    }  
    public Integer pageNumber  
    {  
        get  
        {  
            return con.getPageNumber();  
        }  
        set;  
    }  
    public void previous()  
    {  
        con.previous();  
    }
    public void next()  
    {
        con.next();
    }
    public void goToPage(){
        con.setPageNumber(pageToOpen);
    }
    public Boolean isFirst
    {
        get
        {
	        return con.getPageNumber() == 1;
            
        }
        set;
    }
    public Boolean isLast
    {
        get
        {
            Integer totalPages = con.getResultSize() / con.getPageSize();
            if(Math.mod(con.getResultSize(), con.getPageSize()) > 0)
            {
                totalPages++;
            }
            return con.getPageNumber() == totalPages;            
        }
        set;
    }
    public Integer[] pageIndexArray
    {
        get
        {
            Integer mod = Math.mod(con.getResultSize(), con.getPageSize());
            Integer pageCount = con.getResultSize() / con.getPageSize(); 
            if(mod != 0)
            {
                pageCount++;
            }
            pageIndexArray = new List<Integer>();
            for(Integer i = 1; i <= pageCount; i++)
            {
                pageIndexArray.add(i);
            }
            return pageIndexArray;
        }
        set;
    }
}