public class OrderLineItem {

    public Id productId{get;set;}
    public Integer quantity{get;set;}
    public String name{get;set;}
    public Decimal totalPrice{get;set;}
    
}