public class NewCaseListController {
    public List<Case> getNewCases(){
        List<Case> newCases = [Select ID, CaseNumber from case where status = 'New'];
        return newCases;
    }
}