@isTest
private class TestStudentViewController
{
    @isTest static void test()
    {
        Student__c student = prepareData();
        ApexPages.StandardController sController = new ApexPages.StandardController(student);
        StudentViewController controller = new StudentViewController(sController);
		controller.savePdf();
        List<Attachment> existingAttachments = [SELECT Name FROM Attachment WHERE ParentId = :student.Id];
		System.assert(existingAttachments.size() == 1);
    }

    private static Student__c prepareData()
    {
        Class__c classObj = DataFactory.getDummyClass('MCA');
        insert classObj;

        Student__c student = DataFactory.getDummyStudent(classObj.Id);
        insert student;
        
        insertAttachment(student.Id);
        return student;
    }

    private static Attachment insertAttachment(Id studentId)
    {
        Attachment attach = new Attachment();
        attach.Body = Blob.valueOf('Some Text');
        // add the user entered name
        attach.Name = 'Abc.pdf';
        attach.IsPrivate = false;
        attach.ParentId = studentId;
        insert attach;
        return attach;
    }
}