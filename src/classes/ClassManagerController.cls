public class ClassManagerController
{
    public Class__c classObj{get;set;}
    public void setClass()
    {
        String classId = Apexpages.currentPage().getParameters().get('classId');
        classObj = [SELECT Name,Board__c,Fee__c,Detailed_Description__c,Max_Limit__c FROM Class__c WHERE Id = :classId];
    }
    
    public List<Class__c> getClasses()
    {
        return [SELECT Name,Board__c,Fee__c,Number_Of_Students__c ,Detailed_Description__c,Max_Limit__c FROM Class__c];
    }
    
    public void save()
    {
		update classObj;
		classObj = null;
    }
    
    public void deleteClass()
    {
        if(classObj != null && classObj.Id != null)
        {
            delete classObj;
            classObj = null;
        }
    }
}