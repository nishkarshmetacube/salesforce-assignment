@isTest
private class TestJsonDataController
{
    @isTest static void test()
    {
        JsonDataController controller = new JsonDataController();
        String jsonString = controller.jsonString;
        List<Student> students = (List<Student>) JSON.deserialize(jsonString, List<Student>.class);
		//check students list size
        System.assertEquals(2, students.size());
        
        // check students attributes
		System.assertEquals('Abhishek Gautam', students.get(0).Name);
		System.assertEquals('Jai Prakash', students.get(0).fatherName);
        
        // check nested object
		System.assertEquals('Manav Bharti School', students.get(0).school.name);
    }
}