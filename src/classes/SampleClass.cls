public class SampleClass
{
    private String name{get;set;}

    public SampleClass(String s)
    {
        this.name = s;
    }

    public String name()
    {
        return name;
    }
}