@isTest
private class TestDateUtil
{
    @isTest static void testGetNumberOfWeekDays()
    {
        Date d1 = Date.newInstance(2019, 2, 1);
		Date d2 = d1.addDays(5);
        Integer workingDays = DateUtil.getNumberOfWeekDays(d1, d2);
        System.assertEquals(4, workingDays);

        workingDays = DateUtil.getNumberOfWeekDays(d2, d1);
        System.assertEquals(4, workingDays);
    }
}