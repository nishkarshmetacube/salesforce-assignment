public class XMLparse
{
    String XMLString;
    public List<Contact> pro;
    Contact temppro;

    public XMLparse(String str)
    {
        pro = new List<Contact>();
        XMLString = str;
        DOM.Document doc = new DOM.Document();
        try
        {
            doc.load(XMLString);
            DOM.XmlNode rootNode = doc.getRootElement();
            parseXML(rootNode);
            pro.add(temppro);
            insert pro;
        }
        catch(exception e)
        {
            system.debug(e.getMessage());
        }
    }
    
    private void parseXML(DOM.XMLNode node)
    {
        if (node.getNodeType() == DOM.XMLNodeType.ELEMENT)
        {
            if(node.getName() == 'contact')
            {
                if(temppro != null)
                {
                    pro.add(temppro);                    
                }
                temppro = new Contact();
            }
            if(node.getName() == 'name')
                temppro.lastName = node.getText().trim();
            if(node.getName() == 'account')
                temppro.AccountId = node.getText().trim();
        }
        for (Dom.XMLNode child: node.getChildElements())
        {
            parseXML(child);
        }
    }
}