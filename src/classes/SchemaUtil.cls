public class SchemaUtil 
{
    public static List<String> getAllsObjectNames()
    {
        List<String> sObjectList = new List<String>();
        for(Schema.SObjectType objTyp : Schema.getGlobalDescribe().values())
        {
            String name = objTyp.getDescribe().getName();
            sObjectList.add(name);
 		}
        return sObjectList;
    }
}