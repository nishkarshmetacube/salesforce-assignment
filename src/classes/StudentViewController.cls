public class StudentViewController
{
    public Student__c student {get;set;}
    public String studentId{get;set;}

    public StudentViewController(ApexPages.StandardController controller)
    {
        student = (Student__c) controller.getRecord();
        studentId = student.Id;
        //savePdf();
    }

    public void savePdf() 
    {
        // first, check existing attachment
        List<Attachment> existingAttachments = [SELECT Name FROM Attachment WHERE ParentId = :studentId];
        if(existingAttachments.size() > 0)
        {
          delete existingAttachments;
        }

        // create the new attachment
        Attachment attach = new Attachment();

        // the contents of the attachment from the pdf
        Blob body;
        PageReference page = Page.StudentPdfView;
        page.getParameters().put('id', studentId);

        try 
        {
            // returns the output of the page as a PDF
            body = page.getContent();
        } catch (VisualforceException e) {
            body = Blob.valueOf('Some Text');
        }

        attach.Body = body;
        // add the user entered name
        attach.Name = 'Student Detail.pdf';
        attach.IsPrivate = false;
        // attach the pdf to the account
        attach.ParentId = studentId;
        insert attach;
    }
}