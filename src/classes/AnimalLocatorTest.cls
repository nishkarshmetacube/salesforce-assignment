@isTest
private class AnimalLocatorTest {

    @isTest static void testAnimalName(){
        Test.setMock(HttpCalloutMock.class, new AnimalLocatorMock());
        String animalName = AnimalLocator.getAnimalNameById(1);
        System.assertEquals('chicken', animalName);
    }
    
    @isTest static void testAnimalNameForRequestFailure(){
        Test.setMock(HttpCalloutMock.class, new AnimalLocatorMock(TRUE));
        String animalName = AnimalLocator.getAnimalNameById(1);
        System.assertEquals('chicken', animalName);
    }
}