public class ContactSearch {
    public static List<Contact> searchForContacts(String lastName, String postalCode){
        List<Contact> contactList = [SELECT Id,name FROM Contact WHERE (LastName like :lastName and MailingPostalCode like :postalCode)];
        return contactList;
    }
}