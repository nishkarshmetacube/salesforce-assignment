public class ContactAndLeadSearch {
    public static List<List<SObject>> searchContactsAndLeads(String lastName){
        List<List<SObject>> objList = [FIND :lastName IN NAME FIELDS RETURNING Lead(FirstName,LastName),Contact(FirstName,LastName)]; 
        return objList;
    }
}