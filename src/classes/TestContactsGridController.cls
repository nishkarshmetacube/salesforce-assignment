@isTest
private class TestContactsGridController
{

    @isTest static void test()
    {
        List<Contact> contacts = prepareData();
        ContactsGridController controller = new ContactsGridController();
        testDefaultSearch(controller);
        testToggleSort(controller);
        testSortField(controller);
        testPagination(controller);
    }

    private static List<Contact> prepareData()
    {
		Account acc = DataFactory.getDummyAccount('Nishkarsh');
        List<Contact> contacts = new List<Contact>();
        for(Integer i = 0; i < 11; i++)
        {
            Contact con = DataFactory.getDummyContact('Manav' + i, acc.Id);
            con.Email = 'manav' + i + '@gmail.com';
            contacts.add(con);
        }
        insert contacts;
        return contacts;
    }
    
    private static void testPagination(ContactsGridController controller)
    {
        controller.runQuery();
		System.assert(controller.hasNext);
		System.assert(!controller.hasPrevious);
        controller.next();
        System.assert(controller.hasPrevious);
        System.assert(!controller.hasNext);
        controller.previous();
		System.assert(controller.hasNext);
		System.assert(!controller.hasPrevious);
        controller.last();
        System.assert(controller.hasPrevious);
        System.assert(!controller.hasNext);
        controller.first();
		System.assert(controller.hasNext);
		System.assert(!controller.hasPrevious);
        System.assertEquals(2, controller.pageIndexArray.size());
        controller.pageToOpen = 2;
        controller.goToPage();
        System.assert(controller.hasPrevious);
        System.assert(!controller.hasNext);
        System.assertEquals(2, controller.pageNumber);
    }

    private static void testToggleSort(ContactsGridController controller)
    {
        List<Contact> contacts = [SELECT Name, Title, Email, Phone FROM Contact ORDER BY Name DESC LIMIT 10];
        controller.toggleSort();
        controller.runQuery();
        for(Integer i = 0; i < contacts.size(); i++)
        {
            System.assert(contacts.get(i).Name == controller.contacts.get(i).Name);
        }
    }

    private static void testDefaultSearch(ContactsGridController controller)
    {
        List<Contact> contacts = [SELECT Name, Title, Email, Phone FROM Contact ORDER BY Name ASC LIMIT 10];
        for(Integer i = 0; i < contacts.size(); i++)
        {
            System.assert(contacts.get(i).Name == controller.contacts.get(i).Name);
        }
    }

    private static void testSortField(ContactsGridController controller)
    {
        List<Contact> contacts = [SELECT Name, Title, Email, Phone FROM Contact ORDER BY Email DESC LIMIT 10];
        controller.sortField = 'Email';
        controller.runQuery();
        for(Integer i = 0; i < contacts.size(); i++)
        {
            System.assert(contacts.get(i).Name == controller.contacts.get(i).Name);
        }
    }

}