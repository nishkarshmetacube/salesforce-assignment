@isTest
private class TestAccountPaginationController
{
    @isTest static void test()
    {
        prepareData();
        PageReference page = Page.AccountSearchWithPagination;
        Test.setCurrentPage(page);

        AccountPaginationController controller = new AccountPaginationController();

        testDefaultSearch(controller);
		testToggleSort(controller);
        testSortField(controller);
		testInvalidSortField(controller);
        controller.sortField = 'Name';
        testSearchFields(controller, page);
        testPagination(controller);
    }

    private static void testPagination(AccountPaginationController controller)
    {
        controller.runSearch();
        controller.con.setPageSize(2);
		System.assert(controller.hasNext);
		System.assert(!controller.hasPrevious);
        controller.next();
        System.assert(controller.hasPrevious);
        System.assert(!controller.hasNext);
        controller.previous();
		System.assert(controller.hasNext);
		System.assert(!controller.hasPrevious);
        controller.last();
        System.assert(controller.hasPrevious);
        System.assert(!controller.hasNext);
        controller.first();
		System.assert(controller.hasNext);
		System.assert(!controller.hasPrevious);
    }

    private static void testSearchFields(AccountPaginationController controller, PageReference page)
    {
        List<Account> accounts = [SELECT Name, AccountNumber FROM Account WHERE BillingCity = 'Ateli' ORDER BY AccountNumber DESC LIMIT 100];
        page.getParameters().put('BillingCity','Ateli');
        page.getParameters().put('BillingState','');
        page.getParameters().put('BillingCountry','');
        controller.runSearch();
        System.assert(controller.accounts.size() == accounts.size());
        for(Integer i = 0; i < accounts.size(); i++)
        {
            System.assert(accounts.get(i).Name == controller.accounts.get(i).Name);
        }

        accounts = [SELECT Name, AccountNumber FROM Account WHERE BillingState = 'Haryana' ORDER BY AccountNumber DESC LIMIT 100];
        page.getParameters().put('BillingCity','');
        page.getParameters().put('BillingState','Haryana');
        page.getParameters().put('BillingCountry','');
        controller.runSearch();
        System.assert(controller.accounts.size() == accounts.size());
        for(Integer i = 0; i < accounts.size(); i++)
        {
            System.assert(accounts.get(i).Name == controller.accounts.get(i).Name);
        }

        accounts = [SELECT Name, AccountNumber FROM Account WHERE BillingCountry = 'India' ORDER BY AccountNumber DESC LIMIT 100];
        page.getParameters().put('BillingCity','');
        page.getParameters().put('BillingState','');
        page.getParameters().put('BillingCountry','India');
        controller.runSearch();
        System.assert(controller.accounts.size() == accounts.size());
        for(Integer i = 0; i < accounts.size(); i++)
        {
            System.assert(accounts.get(i).Name == controller.accounts.get(i).Name);
        }        
        page.getParameters().put('BillingCountry','');
    }

    private static void testToggleSort(AccountPaginationController controller)
    {
        List<Account> accounts = [SELECT Name, AccountNumber FROM Account ORDER BY Name DESC LIMIT 100];
        controller.toggleSort();
        controller.runQuery();
        for(Integer i = 0; i < accounts.size(); i++)
        {
            System.assert(accounts.get(i).Name == controller.accounts.get(i).Name);
        }
    }

    private static void testDefaultSearch(AccountPaginationController controller)
    {
        List<Account> accounts = [SELECT Name, AccountNumber FROM Account ORDER BY Name ASC LIMIT 10];
        controller.runQuery();
        System.assert(controller.accounts.size() == accounts.size());
        for(Integer i = 0; i < accounts.size(); i++)
        {
            System.assert(accounts.get(i).Name == controller.accounts.get(i).Name);
        }
    }

    private static void testSortField(AccountPaginationController controller)
    {
        List<Account> accounts = [SELECT Name, AccountNumber FROM Account ORDER BY AccountNumber DESC LIMIT 100];
        controller.sortField = 'AccountNumber';
        controller.runQuery();
        for(Integer i = 0; i < accounts.size(); i++)
        {
            System.assert(accounts.get(i).Name == controller.accounts.get(i).Name);
        }
    }

    private static void testInvalidSortField(AccountPaginationController controller)
    {
        controller.sortField = 'x';
        controller.runQuery();
    }

    private static void prepareData()
    {
        List<Account> accounts = new List<Account>();
        Account acc = DataFactory.getDummyAccount('Nishkarsh');
        acc.BillingCity = 'Ateli';
        accounts.add(acc);

        Account acc1 = DataFactory.getDummyAccount('Manav');
        acc1.BillingState = 'Haryana';
        accounts.add(acc1);

        Account acc2 = DataFactory.getDummyAccount('Jai');
        acc2.BillingCountry = 'India';
        accounts.add(acc2);

        Account acc3 = DataFactory.getDummyAccount('Anurag');
        accounts.add(acc3);
        insert accounts;
    }
}