public class AccountReportController
{
    public AggregateResult[] groupedResults{get;set;}
    public AccountReportController()
    {
        groupedResults = [SELECT AccountId, SUM(Amount)oppSum FROM Opportunity GROUP BY AccountId LIMIT 10];
    }
}