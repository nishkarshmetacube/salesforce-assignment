public class DateUtil 
{
    public static Integer getNumberOfWeekDays(Date d1, Date d2)
    {
        Integer totalDaysBetween = d1.daysBetween(d2);
        Integer totalWorkingDays = 0;
        if(totalDaysBetween<0)
        {
            Date tmp = d2;
            d2 = d1;
            d1 = tmp;
            totalDaysBetween = -totalDaysBetween;
        }
        Integer weekDay = getWeekDay(d1);
        for(Integer i = 1; i < totalDaysBetween; i++)
        {
            weekDay = (weekDay == 7) ? 0 : weekDay+1;
            if(weekDay != 0 && weekDay != 7){
                totalWorkingDays++;
            }
        }
        return totalWorkingDays;
    }

    private static Integer getWeekDay(Date d)
    {
        Date standardDate = Date.newInstance(1900, 1, 7);
		return math.mod(d.daysBetween(standardDate), 7);
    }

}