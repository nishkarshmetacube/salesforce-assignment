public class Teachers extends fflib_SObjectDomain {
    public Teachers(List<Contact> sObjectList)
    {
        super(sObjectList);
    }
    public class Constructor implements fflib_SObjectDomain.IConstructable
    {
        public fflib_SObjectDomain construct(List<SObject> sObjectList)
        {
            return new Teachers(sObjectList);
        }
    }

    public override void onBeforeInsert() 
    {
        preventHindiTeacherUpdation();
    }

    public override void onBeforeUpdate(Map<Id,SObject> existingRecords) 
    {
        preventHindiTeacherUpdation();
    }

    private void preventHindiTeacherUpdation()
    {
        for(Contact teacher : (List<Contact>) records) 
        {
            if(teacher.Subjects__c != null && teacher.Subjects__c.contains('Hindi')) 
            {
                teacher.addError('You cannot insert/update teacher who teach Hindi.');
            }
        }
    }

}