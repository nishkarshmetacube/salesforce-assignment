@isTest
private class TestRestrictContactByName {
    @isTest static void insertInvalidName(){
        Contact c = new Contact(LastName = 'INVALIDNAME',FirstName = 'Nishkarsh' );
        Test.startTest();
        Database.SaveResult result = Database.insert(c, false);
        Test.stopTest();
        system.assert(!result.isSuccess());
        system.assert(result.getErrors().size()>0);
        system.assertEquals('The Last Name "INVALIDNAME" is not allowed for DML', result.getErrors()[0].getMessage());
    }

	@isTest static void insertValidName(){
       Contact c = new Contact(LastName = 'Manav',FirstName = 'Nishkarsh' );
       Test.startTest();
       Database.SaveResult result = Database.insert(c);
       Test.stopTest();
       system.assert(result.isSuccess());
       system.assert(result.getErrors().size()==0);
    }

	@isTest static void updateInvalidName(){
       Contact c = new Contact(LastName = 'Manav',FirstName = 'Nishkarsh' );
        insert c;
       c.LastName = 'INVALIDNAME';
        Test.startTest();
        Database.SaveResult result = Database.update(c, false);
        Test.stopTest();
        system.assert(!result.isSuccess());
        system.assert(result.getErrors().size()>0);
        system.assertEquals('The Last Name "INVALIDNAME" is not allowed for DML', result.getErrors()[0].getMessage());
    }

	@isTest static void updateValidName(){
       Contact c = new Contact(LastName = 'Manav',FirstName = 'Nishkarsh' );
        insert c;
       c.LastName = 'Update Manav';
       Test.startTest();
       Database.SaveResult result = Database.update(c);
       Test.stopTest();
       system.assert(result.isSuccess());
       system.assert(result.getErrors().size()==0);
    }

}