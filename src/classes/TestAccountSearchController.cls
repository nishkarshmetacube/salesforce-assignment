@isTest
private class TestAccountSearchController
{
    @isTest static void test()
    {
        prepareData();
        PageReference page = Page.AccountSearchView;
        Test.setCurrentPage(page);

        List<Account> accounts = [SELECT Name, AccountNumber FROM Account ORDER BY Name ASC LIMIT 100];
        ApexPages.StandardController accountStandardController = new ApexPages.StandardController(accounts.get(0));
        AccountSearchController controller = new AccountSearchController(accountStandardController);

        testDefaultSearch(controller, accounts);
		testToggleSort(controller);
        testSortField(controller);
		testInvalidSortField(controller);
        controller.sortField = 'Name';
        testBillingFields(controller, page);
    }

    private static void testBillingFields(AccountSearchController controller, PageReference page)
    {
        List<Account> accounts = [SELECT Name, AccountNumber FROM Account WHERE BillingCity = 'Ateli' ORDER BY AccountNumber DESC LIMIT 100];
        page.getParameters().put('BillingCity','Ateli');
        page.getParameters().put('BillingState','');
        page.getParameters().put('BillingCountry','');
        controller.runSearch();
        System.assert(controller.accounts.size() == accounts.size());
        for(Integer i = 0; i < accounts.size(); i++)
        {
            System.assert(accounts.get(i).Name == controller.accounts.get(i).Name);
        }

        accounts = [SELECT Name, AccountNumber FROM Account WHERE BillingState = 'Haryana' ORDER BY AccountNumber DESC LIMIT 100];
        page.getParameters().put('BillingCity','');
        page.getParameters().put('BillingState','Haryana');
        page.getParameters().put('BillingCountry','');
        controller.runSearch();
        System.assert(controller.accounts.size() == accounts.size());
        for(Integer i = 0; i < accounts.size(); i++)
        {
            System.assert(accounts.get(i).Name == controller.accounts.get(i).Name);
        }

        accounts = [SELECT Name, AccountNumber FROM Account WHERE BillingCountry = 'India' ORDER BY AccountNumber DESC LIMIT 100];
        page.getParameters().put('BillingCity','');
        page.getParameters().put('BillingState','');
        page.getParameters().put('BillingCountry','India');
        controller.runSearch();
        System.assert(controller.accounts.size() == accounts.size());
        for(Integer i = 0; i < accounts.size(); i++)
        {
            System.assert(accounts.get(i).Name == controller.accounts.get(i).Name);
        }        
        page.getParameters().put('BillingCountry','');
    }

    private static void testToggleSort(AccountSearchController controller)
    {
        List<Account> accounts = [SELECT Name, AccountNumber FROM Account ORDER BY Name DESC LIMIT 100];
        controller.toggleSort();
        controller.runQuery();
        for(Integer i = 0; i < accounts.size(); i++)
        {
            System.assert(accounts.get(i).Name == controller.accounts.get(i).Name);
        }
    }

    private static void testDefaultSearch(AccountSearchController controller, List<Account> accounts)
    {
        controller.runQuery();
        System.assert(controller.accounts.size() == accounts.size());
        for(Integer i = 0; i < accounts.size(); i++)
        {
            System.assert(accounts.get(i).Name == controller.accounts.get(i).Name);
        }
    }

    private static void testSortField(AccountSearchController controller)
    {
        List<Account> accounts = [SELECT Name, AccountNumber FROM Account ORDER BY AccountNumber DESC LIMIT 100];
        controller.sortField = 'AccountNumber';
        controller.runQuery();
        for(Integer i = 0; i < accounts.size(); i++)
        {
            System.assert(accounts.get(i).Name == controller.accounts.get(i).Name);
        }
    }

    private static void testInvalidSortField(AccountSearchController controller)
    {
        controller.sortField = 'x';
        controller.runQuery();
    }

    private static void prepareData()
    {
        List<Account> accounts = new List<Account>();
        Account acc = DataFactory.getDummyAccount('Nishkarsh');
        acc.BillingCity = 'Ateli';
        accounts.add(acc);

        Account acc1 = DataFactory.getDummyAccount('Manav');
        acc1.BillingState = 'Haryana';
        accounts.add(acc1);

        Account acc2 = DataFactory.getDummyAccount('Jai');
        acc2.BillingCountry = 'India';
        accounts.add(acc2);

        Account acc3 = DataFactory.getDummyAccount('Anurag');
        accounts.add(acc3);
        insert accounts;
    }
}