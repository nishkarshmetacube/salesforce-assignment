@isTest
public class DataFactory {

    public static Account getDummyAccount(String name)
    {
        Account acc = new Account();
        acc.Name = name;
        return acc;
    }

    public static Contact getDummyContact(String lastName, Id accId)
    {
        Contact con = new Contact();
        con.LastName = lastName;
        con.AccountId = accId;
        return con;
    }

    public static Opportunity getDummyOpportunity(String name)
    {
        Opportunity opp = new Opportunity();
		opp.Name = name;
        opp.StageName = 'Prospecting';
        opp.CloseDate = Date.today();
        return opp;
    }

	public static Class__c getDummyClass(String name)
    {
        Class__c classObj = new Class__c();
        classObj.Name = name;
        return classObj;
    }

    public static Student__c getDummyStudent(Id classId)
    {
        return getDummyStudent(classId, 'Nishkarsh');
    }

    public static Student__c getDummyStudent(Id classId, String studentName)
    {
		Student__c student = new Student__c();
        student.Name = studentName;
		student.Class__c = classId;
        return student;
    }

    public static List<Student__c> getDummyStudentList(Id classId, Integer totalStudents)
    {
        List<Student__c> students = new List<Student__c>();
        for(Integer i = 1; i <= totalStudents; i++)
        {
            students.add(getDummyStudent(classId, 'Test ' + i));
        }
        return students;
    }
    
}