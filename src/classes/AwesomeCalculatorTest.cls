@isTest
private class AwesomeCalculatorTest {
    @isTest static void testCallout() {              
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new CalculatorCalloutMock());
        // Call the method that invokes a callout
		String country = 'India';
        String[] result = ParkLocator.country(country);
        System.assertEquals(3, result.size()); 
    }
}