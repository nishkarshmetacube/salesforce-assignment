public class DeleteStudentsController
{
    public List<Student__c> students{get;set;}
    public DeleteStudentsController(ApexPages.StandardSetController sController)
    {
        students = (List<Student__c>) sController.getSelected();
        if(students.size() == 0)
        {
            throw new CustomException('No student selected.');
        }
    }
    
    public PageReference deleteStudents()
    {
        delete students;
        return goBack();
    }
    
    public PageReference goBack()
    {
        return new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
    }
    private class CustomException extends Exception{}
}