public with sharing class AccountSearchController
{
	// the soql without the order and limit
  	private String soql {get;set;}
	// the collection of accounts to display
  	public List<Account> accounts {get;set;}
    private static final String ASCENDING_ORDER = 'ASC';
	private static final String DESCENDING_ORDER = 'DESC';

  // the current sort direction. defaults to asc
  public String sortDir
  {
    get  { if (sortDir == null) {  sortDir = ASCENDING_ORDER; } return sortDir;  }
    set;
  }

    // the current field to sort by. defaults to last name
    public String sortField
    {
	    get  { if (sortField == null) {sortField = 'Name'; } return sortField;  }
    	set;
    }

  // init the controller and display some sample data when the page loads
  	public AccountSearchController(ApexPages.StandardController controller)
    {
	    soql = 'SELECT Name, AccountNumber FROM Account WHERE Name != null';
    	runQuery();
  	}

  // toggles the sorting of query from asc<-->desc
  public void toggleSort()
  {
    // simply toggle the direction
    sortDir = sortDir.equals(ASCENDING_ORDER) ? DESCENDING_ORDER : ASCENDING_ORDER;
    // run the query again
    runQuery();
  }
  
  // runs the actual query
  public void runQuery()
  {
    try
    {
		accounts = Database.query(soql + ' ORDER BY ' + sortField + ' ' + sortDir + ' LIMIT 100');
    }
    catch (Exception e)
    {
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Ooops!'));
    }
  }
  
  // runs the search with parameters passed via Javascript
  public PageReference runSearch()
  {
    String billingCity = Apexpages.currentPage().getParameters().get('BillingCity');
    String billingState = Apexpages.currentPage().getParameters().get('BillingState');
    String billingCountry = Apexpages.currentPage().getParameters().get('BillingCountry');

    soql = 'SELECT Name, AccountNumber FROM Account WHERE Name != null';
    if (!billingCity.equals(''))
      soql += ' AND BillingCity LIKE \'%'+billingCity+'%\'';
    if (!billingState.equals(''))
      soql += ' AND BillingState LIKE \'%'+billingState+'%\'';
    if (!billingCountry.equals(''))
      soql += ' AND BillingCountry LIKE \'%'+billingCountry+'%\'';
    // run the query again
    runQuery();
    return null;
  }

}