@isTest
private class TestSchemaUtil {

    @isTest static void testSchemaNames(){
        List<String> sObjectNames = SchemaUtil.getAllsObjectNames();
        System.assert(sObjectNames.contains('Class__c'));
    }
}