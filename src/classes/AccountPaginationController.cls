public class AccountPaginationController
{
	// the soql without the order and limit
    private String soql {get;set;}
    private static String baseQuery = 'SELECT Name, BillingCity, BillingState, BillingCountry, AccountNumber FROM Account WHERE Name != null';
    public List<Account> accounts
    {
        get
        {
            return (List<Account>) con.getRecords();
        }
        set;
    }
	// the collection of accounts to display
    private static final String ASCENDING_ORDER = 'ASC';
	private static final String DESCENDING_ORDER = 'DESC';

    public ApexPages.StandardSetController con { get; set; }

    // the current sort direction. defaults to asc
    public String sortDir
    {
    	get  { if (sortDir == null) {  sortDir = ASCENDING_ORDER; } return sortDir;  }
    	set;
    }
    // the current field to sort by. defaults to last name
    public String sortField
    {
	    get  { if (sortField == null) {sortField = 'Name'; } return sortField;  }
    	set;
    }

  // init the controller and display some sample data when the page loads
  	public AccountPaginationController() {
	    soql = baseQuery;
    	runQuery();
  	}

    // toggles the sorting of query from asc<-->desc
    public void toggleSort()
    {
        // simply toggle the direction
        sortDir = sortDir.equals(ASCENDING_ORDER) ? DESCENDING_ORDER : ASCENDING_ORDER;
        // run the query again
        runQuery();
    }
    
    // runs the actual query
    public void runQuery()
    {
        try
        {
            con = new ApexPages.StandardSetController(Database.getQueryLocator(soql + ' ORDER BY ' + sortField + ' ' + sortDir));
            con.setPageSize(10);
        }
        catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Ooops!'));
        }
    }

    // runs the search with parameters passed via Javascript
    public PageReference runSearch() 
    {
        String billingCity = Apexpages.currentPage().getParameters().get('BillingCity');
        String billingState = Apexpages.currentPage().getParameters().get('BillingState');
        String billingCountry = Apexpages.currentPage().getParameters().get('BillingCountry');
        
        soql = baseQuery;
        if (!billingCity.equals(''))
          soql += ' AND BillingCity LIKE \'%' + billingCity + '%\'';
        if (!billingState.equals(''))
          soql += ' AND BillingState LIKE \'%' + billingState + '%\'';
        if (!billingCountry.equals(''))
          soql += ' AND BillingCountry LIKE \'%'  +billingCountry + '%\'';
        // run the query again
        runQuery();
        return null;
    }

   	// returns the first page of records
 	public void first() 
    {
 		con.first();
 	}

 	// returns the last page of records
 	public void last() 
    {
 		con.last();
 	}

    public Boolean hasNext  
    {  
        get  
        {
            return con.getHasNext();
        }
        set;
    }
    public Boolean hasPrevious
    {
        get
        {
            return con.getHasPrevious();
        }
        set;
    }
    public void previous()
    {
        con.previous();
    }
    public void next()
    {  
        con.next();
    }
    public Boolean isFirst
    {
        get
        {
	        return con.getPageNumber() == 1;
            
        }
        set;
    }
    public Boolean isLast
    {
        get
        {
            Integer totalPages = con.getResultSize() / con.getPageSize();
            if(Math.mod(con.getResultSize(), con.getPageSize()) > 0)
            {
                totalPages++;
            }
            return con.getPageNumber() == totalPages;            
        }
        set;
    }
}