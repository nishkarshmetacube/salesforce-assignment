@isTest
private class TestDeleteStudentsController 
{
    @isTest static void test()
    {
        List<Student__c> students = prepareData();
        String prefix = Schema.getGlobalDescribe().get('Student__c').getDescribe().getKeyPrefix();
		String listingPageUrl = '/' + prefix;
        PageReference page = Page.DeleteStudents;
        page.getParameters().put('retURL', listingPageUrl);
        Test.setCurrentPage(page);
        ApexPages.StandardSetController sController = new ApexPages.StandardSetController(students);
        String errorMsg = '';
        try
        {
	        DeleteStudentsController controller = new DeleteStudentsController(sController);
        }
        catch(Exception e)
        {
            errorMsg = e.getMessage();
        }
        System.assertEquals('No student selected.', errorMsg);
        sController.setSelected(students);
		DeleteStudentsController controller = new DeleteStudentsController(sController);

        // Check all students set properly in controller
        System.assertEquals(students.size(), controller.students.size());

        // check delete functionality
        page = controller.deleteStudents();
        students = [SELECT Name FROM Student__c];
        System.assertEquals(0, students.size());
        System.assert(page.getUrl().contains(listingPageUrl));
    }
    
    private static List<Student__c> prepareData()
    {
        Class__c classObj = DataFactory.getDummyClass('MCA');
        insert classObj;
        List<Student__c> students = DataFactory.getDummyStudentList(classObj.Id, 10);
        insert students;
        return students;
    }
}