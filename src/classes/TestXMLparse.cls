@isTest
private class TestXMLparse 
{
    @isTest static void test()
    {
        Account acc = DataFactory.getDummyAccount('Test');
        insert acc;
        String sampleXml = '<?xml version="1.0" encoding="UTF-8"?><contacts><contact><name>Xbox</name><account>'+acc.Id+'</account></contact><contact><name>UBox</name><account>'+acc.Id+'</account></contact></contacts>';
        XMLparse parse = new XMLparse(sampleXml);
        List<Contact> contacts = [SELECT Name FROM Contact ORDER BY Name];
        System.assertEquals(2, contacts.size());
        System.assertEquals('Xbox', contacts[1].Name);
    }
}