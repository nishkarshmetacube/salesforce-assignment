public class ApexLanguageAssignment 
{
    public static void updateContactIds(Id contactId)
    {
		Contact contactObj = [SELECT Account.ContactIDs__c FROM Contact WHERE Id = :contactId];
        Account accountObj = contactObj.Account;
        if(null == accountObj.ContactIDs__c)
        {
            String contactIdString = '';
            Id accountId = contactObj.Account.Id;
            for(Contact relatedContact : [SELECT Id FROM Contact WHERE Account.Id = :accountId])
            {
				contactIdString += (relatedContact.Id + ',');
            }
            contactIdString = contactIdString.removeEnd(',');
            accountObj.ContactIDs__c = contactIdString;
        }
        else
        {
            String existingContactId = accountObj.ContactIDs__c;
            if(!existingContactId.contains(contactId + ''))
            {
                existingContactId += (',' + contactId);
            }
            accountObj.ContactIDs__c = existingContactId;
        }
        update accountObj;
    }
}