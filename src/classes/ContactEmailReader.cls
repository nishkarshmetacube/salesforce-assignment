global class ContactEmailReader implements Messaging.InboundEmailHandler 
{
	global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email, 
                                                       Messaging.InboundEnvelope env)
    {

        // Create an InboundEmailResult object for returning the result of the 
        // Apex Email Service
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();

            // Save attachments, if any
        for (Messaging.Inboundemail.TextAttachment tAttachment : email.textAttachments)
        {
            if(tAttachment.fileName.endsWith('.xml'))
            {
	        	new XMLparse(tAttachment.body);
            }
        }
        
        // Return the result for the Apex Email Service
        return result;
  }
}