@isTest
private class TestClassSelector
{
    @isTest static void testStudentsCountByClassId()
    {
        List<Class__c> classes = prepareData();
        List<String> studentsInClass1 = ClassSelector.selectStudentsByClassId(Classes.get(0).Id);
        List<String> studentsInClass2 = ClassSelector.selectStudentsByClassId(Classes.get(1).Id);
        System.assertEquals(2, studentsInClass1.size());
        System.assertEquals(0, studentsInClass2.size());
    }

    @isTest static void testGetStudentIdsByClass()
    {
        List<Class__c> classes = prepareData();
        Map<Class__c,Set<Id>> classMap = ClassSelector.getStudentIdsByClass();
        System.assertEquals(2, classMap.size());
        List<Class__c> keys = new List<Class__c>(classMap.keySet());
        Set<Id> studentIds = classMap.get(keys.get(0));
        Integer expectedCount = (keys.get(0).Name == 'Test 1') ? 2 : 0;
        System.assertEquals(expectedCount, studentIds.size());
    }

    private static List<Class__c> prepareData()
    {
        List<Class__c> classList = new List<Class__c>();
        Class__c class1 = new Class__c();
        class1.Board__c = 'RAJ';
        class1.Name = 'Test 1';
        class1.Max_Limit__c = 12;
        classList.add(class1);

        Class__c class2 = new Class__c();
        class2.Board__c = 'CBSE';
        class2.Name = 'Test 2';
        class2.Max_Limit__c = 23;
        classList.add(class2);
        
        insert classList;

        List<Student__c> students = new List<Student__c>();
        Student__c student1 = new Student__c();
        student1.Name = 'Nishkarsh Manav';
        student1.Sex__c = 'male';
        student1.Class__c = class1.Id;
		students.add(student1);

        Student__c student2 = new Student__c();
        student2.Name = 'Jaikishan Sharma';
        student2.Sex__c = 'female';
        student2.Class__c = class1.Id;
		students.add(student2);

        insert students;
        return classList;
    }
}