public class Students extends fflib_SObjectDomain
{
    public Students(List<Student__c> sObjectList)
    {
        super(sObjectList);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable
    {
        public fflib_SObjectDomain construct(List<SObject> sObjectList)
        {
            return new Students(sObjectList);
        }
    }

    public override void onAfterInsert()
    {
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(new Schema.SObjectType[] {Class__c.SObjectType});
        List<AggregateResult> aggregateResults = [SELECT Class__c, Count(Id) StudentsInClass 
                                                  FROM Student__c 
                                                  WHERE Id IN :(List<Student__c>) Records 
                                                  GROUP BY Class__c];
        
        Map<Id, Integer> classMap = new Map<Id, Integer>();
        for(AggregateResult result : aggregateResults)
        {
            classMap.put((Id) result.get('Class__c'), (Integer) result.get('StudentsInClass'));
        }
        
        for(Class__c studentClass: [SELECT Id, MyCount__c FROM Class__c WHERE Id IN :classMap.keySet()])
        {
            studentClass.MyCount__c += classMap.get(studentClass.Id);
            uow.registerDirty(studentClass);
        }
        uow.commitWork();
    }

    public override void onBeforeUpdate(Map<Id,SObject> existingRecords)
    {
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(new Schema.SObjectType[] {Class__c.SObjectType});
        
        Map<Id, Integer> newClassMap = new Map<Id, Integer>();
        List<Student__c> newStudents = new List<Student__c>();

        for(Student__c record : (List<Student__c>) Records )
        {
            Student__c oldRecord = (Student__c) existingRecords.get(record.Id);
            if(record.Class__c != oldRecord.Class__c)
            {
                newStudents.add(record);
                Integer newStudentsCount = newClassMap.get((Id) record.Class__c);
                if(newStudentsCount != null)
                {
                    newClassMap.put((Id) record.Class__c, newStudentsCount + 1);
                }
                else 
                {
                    newClassMap.put((Id) record.Class__c, 1);
                }
            }
        }

        List<AggregateResult> oldAggregateResults = [SELECT Class__c, Count(Id) StudentsInClass 
                                                     FROM Student__c 
                                                     WHERE Id IN :newStudents
                                                     GROUP BY Class__c];
        
        Map<Id, Integer> oldClassMap = new Map<Id, Integer>();
        for(AggregateResult result: oldAggregateResults)
        {
            oldClassMap.put((Id) result.get('Class__c'), (Integer) result.get('StudentsInClass'));
            
        }
        
        for(Class__c studentClass: [SELECT Id, MyCount__c FROM Class__c WHERE Id IN :oldClassMap.keySet()])
        {
            studentClass.MyCount__c -= oldClassMap.get(studentClass.Id);
            uow.registerDirty(studentClass);
        }

        for(Class__c studentClass: [SELECT Id, MyCount__c FROM Class__c WHERE Id IN :newClassMap.keySet()])
        {
            studentClass.MyCount__c += newClassMap.get(studentClass.Id);
            uow.registerDirty(studentClass);
        }

        uow.commitWork();
    }

	public override void onValidate() 
    {
        Map<Id,Student__c> entries = new Map<Id,Student__c>([SELECT Class__r.Name, Class__r.Max_Limit__c, Class__r.Number_Of_Students__c FROM Student__c WHERE Id IN :(List<Student__c>) records]);
        for(Student__c student : (List<Student__c>) records)
        {
            Student__c std = entries.get(student.Id);
            if(std.Class__r.Max_Limit__c != null && std.Class__r.Max_Limit__c <= std.Class__r.Number_Of_Students__c)
            {
                student.addError(std.Class__r.Name+' already have maximum number of students.');
            }
        }
    }

}