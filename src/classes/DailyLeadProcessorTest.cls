@istest
private class DailyLeadProcessorTest {
	public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    @isTest static void testMe(){
        Test.startTest();
        List<Lead> leads = new List<Lead>();
        for(Integer i=0;i<200;i++){
            Lead l = new Lead(lastName='Nishkarsh'+i,company='metacube'+i);
            leads.add(l);
        }
        insert leads;
        System.schedule('Test Job', CRON_EXP,new DailyLeadProcessor());
        Map<Id, Lead> leadMap = new Map<Id, Lead>(leads);
        List<Id> leadIds = new List<Id>(leadMap.keySet());
        List<Task> lt = [SELECT Id FROM Task WHERE WhatId IN :leadIds];
        System.assertEquals(0, lt.size(), 'Tasks exist before job has run');
        // Stopping the test will run the job synchronously
        Test.stopTest();
        
        // Now that the scheduled job has executed,
        // check that our tasks were created
        List<Lead> updatedLeads = [SELECT Id, leadsource FROM Lead WHERE leadsource = 'Dreamforce'];
		System.assertEquals(leads.size(), 
            updatedLeads.size(), 
            'Tasks were not created');
    }
}