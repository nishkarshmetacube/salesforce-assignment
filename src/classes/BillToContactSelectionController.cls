public class BillToContactSelectionController 
{
    public Id contactId{get;set;}
    public Id oppId{get;set;}
    public Id accountId{get;set;}
    public String opportunityLink
    {
        get
        {
            return '/' + oppId;
        }
        set;
    }
    public List<SelectOption> items
    {
        get
        {
            List<SelectOption> options = new List<SelectOption>(); 
            for(Contact con : [SELECT Name FROM contact WHERE Account.Id = :accountId]){
	            options.add(new SelectOption(con.Id,con.Name));
            }
            return options;
        }
        set;
    }
    public BillToContactSelectionController()
    {
        accountId = ApexPages.currentPage().getParameters().get('id');
        oppId = ApexPages.currentPage().getParameters().get('oppId');
    }
    public PageReference save()
    {
        updateContact(contactId);
        return new Pagereference(opportunityLink);
    }
    public PageReference clear()
    {
        updateContact(null);
        return new Pagereference(opportunityLink);        
    }
    public PageReference cancel()
    {
        return new Pagereference(opportunityLink);        
    }
    private void updateContact(Id cId)
    {
        Opportunity opp = [SELECT BillToContact__c FROM Opportunity WHERE Id = :oppId];
        opp.BillToContact__c = cId;
        update opp;
    }
}