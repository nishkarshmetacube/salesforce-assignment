@isTest
private class TestTeachers
{
    @isTest static void testHindiTeacherInsert()
    {
        Contact con = prepareData();
        con.Subjects__c = 'Hindi';
        String errorMsg = '';
        try
        {
	        Database.SaveResult result = Database.insert(con);
        }
        catch(Exception e){
            errorMsg = e.getMessage();
        }
        System.assert(errorMsg.contains('You cannot insert/update teacher who teach Hindi.'));
    }

    @isTest static void testHindiTeacherUpdate()
    {
        Contact con = prepareData();
        con.Subjects__c = 'English';
        insert con;
        con.Subjects__c = 'Hindi';
        String errorMsg = '';
        try
        {
	        Database.SaveResult result = Database.update(con);
        }
        catch(Exception e)
        {
            errorMsg = e.getMessage();
        }
        System.assert(errorMsg.contains('You cannot insert/update teacher who teach Hindi.'));
    }

    private static Contact prepareData()
    {
        Account acc = DataFactory.getDummyAccount('nishkarsh');
        insert acc;
        Contact con = DataFactory.getDummyContact('Manav', acc.Id);
        return con;
    }
}