@isTest
private class TestClasses {
    @isTest static void test(){
		Class__c classObj = prepareData();
        String errorMsg = '';
        try
        {
            delete classObj;
        }
        catch(Exception e)
        {
            errorMsg = e.getMessage();
        }
        System.assert(errorMsg.contains('Cannot delete class which have more than 1 female student.'));
    }
    
    private static Class__c prepareData(){
        Class__c classObj = DataFactory.getDummyClass('MCA');
		insert classObj;

        List<Student__c> students = new List<Student__c>();
        Student__c student = DataFactory.getDummyStudent(classObj.Id);
        student.Sex__c = 'Female';
        students.add(student);

        Student__c student1 = DataFactory.getDummyStudent(classObj.Id);
        student1.Sex__c = 'Female';
        students.add(student1);

        insert students;
        return classObj;
    }
}