public class Stack {
   private Integer maxSize;
   private Long[] stackArray;
   private Integer top;

   public Stack(Integer s) 
   {
      maxSize = s;
      stackArray = new Long[maxSize];
      top = -1;
   }
   public void push(long j) 
   {
       if(top == (maxSize - 1))
       		throw new StackException('Stack is Full.');
      stackArray[++top] = j;
   }
   public long pop()
   {
       if(top == -1)
       		throw new StackException('Stack is Empty.');
      return stackArray[top--];
   }
   public long peek()
   {
      return stackArray[top];
   }
   private class StackException extends Exception{}
}