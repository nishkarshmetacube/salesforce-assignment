public class ProductListingController
{
    public ProductListingController()
    {
        String contactId = ApexPages.currentPage().getParameters().get('id');
        if(String.isBlank(contactId))
        {
            throw new CustomException('Please provide Contact Id in URL.');
        }
        try
        {
	        contact = [SELECT Name, Email, MobilePhone FROM Contact WHERE Id = :contactId];            
        }
        catch(Exception e)
        {
            throw new CustomException('Please provide valid Contact Id.');            
        }
    }

    public Id productToAdd {get;set;}
    private Contact contact;
    public Purchase_Order__c order
    {
        get
        {
            if(order == null){
                order = new Purchase_Order__c();
                order.Contact__c = contact.Id;
                order.Contact__r = contact;
            }
            return order;
        }
        set;
    }
    public Decimal bagCost
    {
        get
        {
            if(tempLineItems.size() == 0){
                return 0;
            }
            else
            {
                bagCost = 0;
                for(OrderLineItem item : tempLineItems)
                {
                    bagCost += item.totalPrice;
                }
                return bagCost;
            }
        }
        set;
    }
    public Map<Id,Product__c> allProducts
    {
        get
        {
            if(allProducts == null)
            {
                allProducts = new Map<Id,Product__c>([SELECT Name, Available_Quantity__c, Price__c From Product__c WHERE Available_Quantity__c > 0]);
            }
            return allProducts;
        }
        set;
    }
    public List<Purchase_Order_LineItem__c> orderLineItems
    {
        get;
        set;
    }

    public Map<Id,OrderLineItem> productsLineItemMap
    {
        get
        {
            if(productsLineItemMap == null){
                productsLineItemMap = new Map<Id,OrderLineItem>();
            }
            return productsLineItemMap;
        }
        set;
    }
    public List<OrderLineItem> tempLineItems
    {
        get
        {
            return productsLineItemMap.values();
        }
        set;
    }
    public List<Product__c> getProducts()
    {
        return allProducts.values();
    }

    public void increaseQuantity()
    {
		Product__c prod = allProducts.get(productToAdd);
        OrderLineItem lineItem = productsLineItemMap.get(prod.Id);
        if(lineItem.quantity < prod.Available_Quantity__c)
        {
            lineItem.quantity++;
            lineItem.totalPrice += prod.Price__c;
        }
        else
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Order quantity must be smaller than available quantity.'));
        }
    }
    
    public void decreaseQuantity()
    {
		Product__c prod = allProducts.get(productToAdd);
        OrderLineItem lineItem = productsLineItemMap.get(prod.Id);
        if(lineItem.quantity == 1)
        {
			productsLineItemMap.remove(prod.Id);
        }
        else
        {
			lineItem.quantity--;
            lineItem.totalPrice -= prod.Price__c;
        }
    }
    public void addProduct()
    {
        if(!productsLineItemMap.containsKey(productToAdd))
        {
            Product__c prod = allProducts.get(productToAdd);
	        OrderLineItem orderLineItem = new OrderLineItem();
            orderLineItem.productId = productToAdd;
            orderLineItem.quantity = 1;
            orderLineItem.name = prod.Name;
            orderLineItem.totalPrice = (prod.Price__c * orderLineItem.quantity);
			productsLineItemMap.put(productToAdd, orderLineItem);
            tempLineItems = null;
        }
        else
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Product has been already added.'));
        }
    }
    
    public PageReference placeOrder()
    {
        return Page.OrderPage;
    }
    public PageReference homePage()
    {
    	productToAdd = null;
        order = null;
        bagCost = 0;
        orderLineItems = null;
        productsLineItemMap = null;
        allProducts = null;
        return Page.ProductListingPage;
    }
    public void savePersonalDetail()
    {
        try
        {
            insert order;
            orderLineItems = new List<Purchase_Order_LineItem__c>();
            List<Product__c> productsToUpdate = new List<Product__c>();
            for(OrderLineItem lineItem : tempLineItems)
            {
                Purchase_Order_LineItem__c poLineItem = getLineItem(lineItem, order.Id);
                Product__c product = allProducts.get(poLineItem.Product__c);
                product.Available_Quantity__c -= poLineItem.Quantity__c;
                productsToUpdate.add(product);
                poLineItem.Product__r = product;
                orderLineItems.add(poLineItem);
            }
            insert orderLineItems;
            update productsToUpdate;
            // Update Order object to fetch actual information to show in Invoice.
            order = [SELECT Number_of_Line_Items__c, Address__c, Total_Price__c FROM Purchase_Order__c WHERE Id = :order.Id];
            order.Contact__r = contact;
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.INFO, 'Congratulations !! Your order has been placed successfully.'));
        }
        catch(Exception e)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Sorry !! Some exception occured. Please place order again.'));
        }
    }

    private Purchase_Order_LineItem__c getLineItem(OrderLineItem lineItem, Id orderId)
    {
        Purchase_Order_LineItem__c item = new Purchase_Order_LineItem__c();
        item.Product__c = lineItem.productId;
        item.Quantity__c = lineItem.quantity;
        item.Purchase_Order__c = orderId;
        return item;
    }
    
    private class CustomException extends Exception{}
}