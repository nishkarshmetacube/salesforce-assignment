@isTest
private class TestApexLanguageAssignment {

    @isTest static void test()
    {
        Id contactId = prepareData();
        ApexLanguageAssignment.updateContactIds(contactId);
        Contact con = [SELECT Account.ContactIDs__c FROM Contact WHERE Id = :contactId];
        Account accountObj = con.Account;
        Id accountId = accountObj.Id;
        System.assertEquals(String.valueOf(contactId), accountObj.ContactIDs__c);

        Contact con1 = DataFactory.getDummyContact('Pandey', accountId);
		insert con1;

        ApexLanguageAssignment.updateContactIds(con1.Id);
        String expected = contactId + ',' + con1.Id;
        accountObj = [SELECT ContactIDs__c FROM Account WHERE Id = :accountId];
        System.assertEquals(expected, accountObj.ContactIDs__c);
    }

    private static Id prepareData()
    {
        Account acc = DataFactory.getDummyAccount('Nishkarsh Manav');
        insert acc;

        Contact con = DataFactory.getDummyContact('Sharma', acc.Id);
		insert con;
        return con.Id;
    }
}