@isTest
global class AnimalLocatorMock implements HttpCalloutMock {

    private Boolean isErrorResponse = False;
    global AnimalLocatorMock(){
        
    }
    
    global AnimalLocatorMock(Boolean isErrorResponse){
        this.isErrorResponse = isErrorResponse;
    }

    global HTTPResponse respond(HTTPRequest request) {
        // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"animal":{"id":1,"name":"chicken","eats":"chicken food","says":"cluck cluck"}}');
        Integer statusCode = (isErrorResponse) ? 500 : 200;
        response.setStatusCode(statusCode);
        return response; 
    }

}