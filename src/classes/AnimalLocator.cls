public class AnimalLocator {
    public static String getAnimalNameById(Integer animalId){
		Http httpObj = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://th-apex-http-callout.herokuapp.com/animals/'+animalId);
        req.setMethod('GET');
        HttpResponse response = httpObj.send(req);
        if(response.getStatusCode() == 200){
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            // Cast the values in the 'animals' key as a list
            Map<String,Object> animalProps = (Map<String,Object>) results.get('animal');
			return (String) animalProps.get('name');
        }
        return '';
    }
}