public class HomePageController
{
    public Map<Id, Student__c> students{get;set;}
    public Student__c student{get;set;}
    public Id seletedStudent{get;set;}
    public List<SelectOption> getItems()
    {
        List<SelectOption> options = new List<SelectOption>();
        for(Id stdId : students.keySet())
        {
	        options.add(new SelectOption(stdId, students.get(stdId).Name));
        }
        return options;
    }

    public HomePageController()
    {
        students = new Map<Id, Student__c>([SELECT Name, First_Name__c, Married__c, DOB__c, Email__c FROM Student__c ORDER BY Name]);
    }
    public void showDetail()
    {
        student = students.get(seletedStudent);
    }
}