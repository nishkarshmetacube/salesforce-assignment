public class StudentNameController 
{
	private Class__c classObj;
    public Student__c student{get;set;}
    public StudentNameController()
    {
        String classId = ApexPages.currentPage().getParameters().get('id');
        try
        {
            classObj = [SELECT Name FROM Class__c WHERE Id = :classId];
            student = new Student__c();
            student.Class__c = classObj.Id;
        }
        catch(Exception e)
        {
            throw new CustomException('Invalid Class Id !');
        }
    }
    public void save()
    {
        insert student;
    }
    private class CustomException extends Exception{}
}