global with sharing class CaseService {
    global static void closeCases(Set<ID> caseIDs, String closeReason){
        List<Case> cases = [select Id,Status,Reason from case where id in :caseIDs];
        for(Case c : cases){
            c.Reason = closeReason;
            c.status = 'Closed'; 
        }
        update cases;
    }
}