@isTest
private class TestStudents
{
    @isTest static void test()
    {
		Class__c classObj = prepareData();
        Student__c student = DataFactory.getDummyStudent(classObj.Id);
        String errorMsg = '';
        try
        {
	        insert student;
        }
        catch(Exception e)
        {
            errorMsg = e.getMessage();
        }
		System.assert(errorMsg.contains('MCA already have maximum number of students.'));
    }

    @isTest static void testMyCount()
    {
		List<Class__c> classes = getClasses();
        CLass__c expectedClass = classes.get(0);
        List<Student__c> students = DataFactory.getDummyStudentList(expectedClass.Id, 10);
        insert students;
        expectedClass = [SELECT Name, MyCount__c FROM Class__c WHERE Id = :expectedClass.Id];

        // check students count updated properly in MyCount variable.
        System.assertEquals(students.size(), expectedClass.MyCount__c);
        
		// Migrate some student to some other class & then check MyCount
		List<Student__c> studentsToUpdate = new List<Student__c>();
		Student__c student1 = students.get(0);
        Id newClassId = classes.get(1).Id;
        student1.Class__c = newClassId;
        studentsToUpdate.add(student1);

		Student__c student2 = students.get(1);
        student2.Class__c = newClassId;
        studentsToUpdate.add(student2);

        update studentsToUpdate;
        Map<Id, Class__c> classMap = new Map<Id, Class__c>([SELECT Name, MyCount__c FROM Class__c WHERE Id IN :classes]);
        System.assertEquals(2, classMap.get(newClassId).MyCount__c);
        System.assertEquals(8, classMap.get(expectedClass.Id).MyCount__c);
    }

    private static List<Class__c> getClasses()
    {
        List<Class__c> classes = new List<Class__c>();
        Class__c class1 = DataFactory.getDummyClass('MCA');
        class1.Max_Limit__c = 10;
        classes.add(class1);

        Class__c class2 = DataFactory.getDummyClass('BCA');
        class2.Max_Limit__c = 2;
        classes.add(class2);

        insert classes;
        return classes;
    }

    private static Class__c prepareData()
    {
        Class__c classObj = DataFactory.getDummyClass('MCA');
        classObj.Max_Limit__c = 1;
        insert classObj;

        Student__c student = DataFactory.getDummyStudent(classObj.Id);
        insert student;
        return classObj;
    }
}