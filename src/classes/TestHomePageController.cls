@isTest
private class TestHomePageController
{
    @isTest static void test()
    {
        List<Student__c> students = getStudents();
        HomePageController controller = new HomePageController();
        System.assertEquals(students.size(), controller.students.size());
        System.assertEquals(students.size(), controller.getItems().size());
        controller.seletedStudent = students.get(0).Id;
        controller.showDetail();
        System.assertEquals(students.get(0).Name, controller.student.Name);
    }
    
    private static List<Student__c> getStudents()
    {
        Class__c classObj = DataFactory.getDummyClass('MCA');
        insert classObj;
        List<Student__c> students = DataFactory.getDummyStudentList(classObj.Id, 10);
        insert students;
        return students;
    }
}