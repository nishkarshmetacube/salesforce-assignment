public class ClassSelector
{
    public static List<String> selectStudentsByClassId(Id classId) 
    {
        List<String> studentNames = new List<String>();
        for(Student__c student : [SELECT Id,Name FROM Student__c WHERE Class__c = :classId])
        {
            studentNames.add(student.Name);
        }
        return studentNames;
    }

    public static Map<Class__c,Set<Id>> getStudentIdsByClass()
    {
        Map<Class__c,Set<Id>> studentMap = new Map<Class__c,Set<Id>>();
        for(Class__c classObj : [SELECT Id,Name,(SELECT Name FROM Students__r) FROM Class__c])
        {
            Set<Id> students = new Set<Id>();
            for(Student__c studentObj : classObj.Students__r)
            {
                students.add(studentObj.Id);
            }
            studentMap.put(classObj, students);
        }
        return studentMap;
    }

}