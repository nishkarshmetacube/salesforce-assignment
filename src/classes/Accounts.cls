public class Accounts extends fflib_SObjectDomain
{
    public Accounts(List<Account> sObjectList)
    {
        super(sObjectList);
    }

    public override void onApplyDefaults()
    {
        for(Account account : (List<Account>) records)
        {
            account.Description = 'Domain classes rock!';
        }
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable
    {
        public fflib_SObjectDomain construct(List<SObject> sObjectList)
        {
            return new Accounts(sObjectList);
        }
    }

    public override void onBeforeUpdate(Map<Id,SObject> existingRecords)
    {
        updateAnnualRevenue(existingRecords);
    }

    private void updateAnnualRevenue(Map<Id,SObject> existingRecords)
    {
        //calculate value and assign to annualrev
        for(Account acc: (List<Account>)Records)
        {
           	Account oldVal = (Account)existingRecords.get(acc.Id);
            String s = oldVal.Description; 
            acc.AnnualRevenue = (acc.Description).getLevenshteinDistance(s);
        }    
    }

    public override void onAfterInsert()
    {
        updateChildAccountsCount();
    }

    public override void onAfterUpdate(Map<Id,SObject> existingRecords)
    {
        updateChildAccountsCount();
    }

    public override void onAfterDelete()
    {
        updateChildAccountsCount();
    }

    private void updateChildAccountsCount()
    {
        AggregateResult[] results = [SELECT Parent__c, COUNT(Id)a FROM Account GROUP BY Parent__c];
        Map<Id,Integer> countByParentId = new Map<Id,Integer>();
        for(AggregateResult res : results)
        {
            countByParentId.put((Id) res.get('Parent__c'), (Integer) res.get('a'));
        }
        Map<Id, Account> parentAccountMap = new Map<Id, Account>();
        for(Account acc : (List<Account>) records)
        {
            if(acc.Parent__c != null && !parentAccountMap.containsKey(acc.Parent__c))
            {
                parentAccountMap.put(acc.Parent__c, acc);
            }
        }
        List<Account> updatedAcc = [SELECT Child_Count__c FROM Account WHERE Id IN :parentAccountMap.keyset()];
        for(Account parentAcc : updatedAcc)
        {
            parentAcc.Child_Count__c = (countByParentId.get(parentAcc.Id) == null) ? 0 : countByParentId.get(parentAcc.Id);
        }
        update updatedAcc;
    }
}