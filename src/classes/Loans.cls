public class Loans extends fflib_SObjectDomain
{
    public Loans(List<Loan__c> sObjectList)
    {
        super(sObjectList);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable
    {
        public fflib_SObjectDomain construct(List<SObject> sObjectList)
        {
            return new Loans(sObjectList);
        }
    }

    public override void onAfterUpdate(Map<Id,SObject> existingRecords)
    {
        List<String> emailIds = new List<String>();
        for(Loan__c loan : (List<Loan__c>) records)
        {
            Loan__c oldRecord = (Loan__c) existingRecords.get(loan.Id);
            if(loan.Status__c != oldRecord.Status__c && loan.Applicant_Email__c != null)
            {
                emailIds.add(loan.Applicant_Email__c);
            }
        }
        if(emailIds.size() > 0)
        {
			EmailManager.sendMail(emailIds, 'Loan Application Status Changed', 'Hi, Application status has been changed.');
        }
    }
}