@isTest
private class LeadProcessorTest {

    @isTest static void testRecordUpdate(){
        Test.startTest();
        List<Lead> totalLeads = new List<Lead>();
        for(Integer i =0;i<200;i++){
            Lead l = new Lead(lastName='nishkarsh',company='metacube');
            totalLeads.add(l);
        }
        insert totalLeads;
        LeadProcessor lp = new LeadProcessor();
        Id batchId = Database.executeBatch(lp);
        Test.stopTest();
        List<Lead> updatedList = [select id from lead where LeadSource ='DreamForce'];
        System.assertEquals(200, updatedList.size());
    }
}