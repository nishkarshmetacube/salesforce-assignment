public class Classes extends fflib_SObjectDomain {
    public Classes(List<Class__c> sObjectList) {
        super(sObjectList);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> sObjectList) {
            return new Classes(sObjectList);
        }
    }

    public override void onBeforeDelete()
    {
        Set<Id> classIds = new Set<Id>();
        for(Class__c c : (List<Class__c>) Records)
        {
            classIds.add(c.Id);
        }
        Map<Id, Class__c> entries = new Map<Id, Class__c>([SELECT (SELECT Name FROM Students__r WHERE Sex__c = 'Female') FROM Class__c WHERE Id IN :classIds]);
        for(Class__c c : (List<Class__c>) records)
        {
            List<Student__c> students = entries.get(c.Id).Students__r;
            if(students.size() > 1)
            {
                c.addError('Cannot delete class which have more than 1 female student.');
            }
        }
    }
}