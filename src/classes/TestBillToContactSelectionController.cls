@isTest
private class TestBillToContactSelectionController
{
    @isTest static void test()
    {
        PageReference page = Page.SelectBillToContact;
        Test.setCurrentPage(page);
        Account acc = createAccount();
        List<Contact> contacts = getContacts(acc.Id);
        Opportunity opp = getOpportunity();
        opp.Manager__c = acc.Id;
        page.getParameters().put('id' , acc.Id);
        page.getParameters().put('oppId', opp.Id);

        BillToContactSelectionController controller = new BillToContactSelectionController();
        System.assertEquals(contacts.size(), controller.items.size());
        Id contactId = contacts.get(0).Id;
		controller.contactId = contactId;

        controller.cancel();
        Opportunity updatedOpp = [SELECT BillToContact__c FROM Opportunity WHERE Id = :opp.Id];
        System.assert(updatedOpp.BillToContact__c == null);

        controller.save();
        updatedOpp = [SELECT BillToContact__c FROM Opportunity WHERE Id = :opp.Id];
        System.assert(updatedOpp.BillToContact__c == contactId);

        PageReference savePage = controller.clear();
        updatedOpp = [SELECT BillToContact__c FROM Opportunity WHERE Id = :opp.Id];
        System.assert(updatedOpp.BillToContact__c == null);
    }

    private static Account createAccount()
    {
        Account acc = DataFactory.getDummyAccount('Nishkarsh');
        insert acc;
        return acc;
    }

    private static List<Contact> getContacts(Id accId){
        List<Contact> contacts = new List<Contact>();
        Contact con1 = DataFactory.getDummyContact('Manav', accId);
        contacts.add(con1);

        Contact con2 = DataFactory.getDummyContact('Sanidhya', accId);
        contacts.add(con2);
        insert contacts;
        return contacts;
    }

    private static Opportunity getOpportunity()
    {
        Opportunity opp = DataFactory.getDummyOpportunity('Opp');
        insert opp;
        return opp;
    }
}