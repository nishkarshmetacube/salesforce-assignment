global class MyWebService
{
    webservice static Contact[] getContacts()
    {
        Contact[] contacts = [SELECT Name FROM contact LIMIT 10];
        return contacts;
    }

    webservice static Id insertStudent(String name, Id classId)
    {
        Student__c std = new Student__c();
        std.Name = name;
        std.Class__c = classId;
        insert std;
        return std.Id;
    }
}