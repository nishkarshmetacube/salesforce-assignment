global class DailyLeadProcessor implements Schedulable {
    global void execute(SchedulableContext ctx) {
        List<Lead> leads = [SELECT Id, leadsource FROM Lead WHERE leadsource = '' limit 200];
        for (Lead l : leads){
            l.LeadSource = 'Dreamforce';
        }
		update leads;
    }
}