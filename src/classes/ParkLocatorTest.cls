@isTest
private class ParkLocatorTest {
    @isTest static void testCallout() {              
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new ParkServiceMock());
        // Call the method that invokes a callout
		String country = 'India';
        Set<String> parks = new Set<String>{'Anamudi Shola National Park', 'Anshi National Park', 'Bandipur National Park'};
        List<String> result = ParkLocator.country(country);
        System.assert(parks.containsAll(result) && result.size() == parks.size());
    }
}